#include "security_policy_manager.hpp"
#include "ntske4ptp/messages.hpp"
#include <set>

namespace ntske4ptp {

std::optional<messages::group_response::parameter_set> get_parameter_set(
    security_parameters const& parameters, group_id id) noexcept {
    auto const found = parameters.spd.find(id);
    if (found == parameters.spd.end()) {
        return std::nullopt;
    }

    messages::group_response::parameter_set result;

    auto const& sp = found->second;
    result.sp = sp;

    std::set<security_parameter_pointer> unique_spps;
    for (auto [_, spp] : sp) {
        unique_spps.insert(spp);
    }

    for (auto spp : unique_spps) {
        result.sas[spp] = parameters.sad.at(spp);
    }

    result.vp = parameters.validity;
    result.update_time = parameters.update_time;

    return result;
}

std::vector<uint8_t> security_policy_manager::process_group_request(
    std::vector<uint8_t> const& request_buffer, x509_ptr client_cert) const {
    auto const request = messages::deserialize_group_request(request_buffer);

    // TODO: check client certificate for authorization (maybe only pass subject name)

    messages::group_response response;

    {
        std::shared_lock lock(mutex_); // reader lock

        if (auto params = get_parameter_set(current_parameters_, request); params.has_value()) {
            response.current_parameters = std::move(*params);
        } else {
            return messages::serialize_group_response(std::nullopt);
        }

        if (std::chrono::system_clock::now() > current_parameters_.update_time) {
            if (auto params = get_parameter_set(next_parameters_, request); params.has_value()) {
                response.next_parameters = std::move(*params);
            } else {
                response.next_parameters = messages::group_response::group_access_denied{};
            }
        }
    }

    return messages::serialize_group_response(std::move(response));
}

} // namespace ntske4ptp
