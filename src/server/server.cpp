#include "security_policy_manager.hpp"
#include "tls_server.hpp"
#include <fmt/format.h>

int main() {
    try {
        asio::io_context io_context;
        ntske4ptp::security_policy_manager policy_manager;
        ntske4ptp::tls_server server(io_context, 8888, policy_manager);
        io_context.run();
    } catch (std::exception const& e) {
        fmt::print("Error: {}\n", e.what());
    }
}
