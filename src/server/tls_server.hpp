#ifndef NTSKE4PTP_SERVER_TLS_SERVER_HPP
#define NTSKE4PTP_SERVER_TLS_SERVER_HPP

#include "security_policy_manager.hpp"
#include <asio/ip/tcp.hpp>
#include <asio/ssl/context.hpp>

namespace ntske4ptp {

class tls_server {
  public:
    tls_server(asio::io_context& io_context, std::uint16_t port, security_policy_manager const& sp_manager);

  private:
    void do_accept();

    asio::ip::tcp::acceptor acceptor_;
    asio::ssl::context context_;
    // needs ref to SPD(_manager?)
    security_policy_manager const& sp_manager_;
};

} // namespace ntske4ptp

#endif
