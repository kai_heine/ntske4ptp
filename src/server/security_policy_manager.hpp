#ifndef NTSKE4PTP_SECURITY_POLICY_MANAGER_HPP
#define NTSKE4PTP_SECURITY_POLICY_MANAGER_HPP

#include "ntske4ptp/security_policy.hpp"
#include <memory>
#include <openssl/x509.h>
#include <shared_mutex>

namespace ntske4ptp {

struct x509_deleter {
    void operator()(X509* ptr) const noexcept { X509_free(ptr); }
};

using x509_ptr = std::unique_ptr<X509, x509_deleter>;

class security_policy_manager {
  public:
    std::vector<std::uint8_t> process_group_request(
        std::vector<std::uint8_t> const& request, x509_ptr client_cert) const;

  private:
    // void update_databases(); // asio timer?

    security_parameters current_parameters_;
    security_parameters next_parameters_;

    mutable std::shared_mutex mutex_;
};

} // namespace ntske4ptp

#endif
