#include "tls_server.hpp"
#include "ntske4ptp/messages.hpp"
#include <asio/read.hpp>
#include <asio/ssl/stream.hpp>
#include <asio/write.hpp>
#include <iostream>

namespace ntske4ptp {

using namespace asio;

// based on asio echo server example
class tls_session : public std::enable_shared_from_this<tls_session> {
  public:
    tls_session(
        ip::tcp::socket socket, asio::ssl::context& context, security_policy_manager const& sp_manager)
        : stream_(std::move(socket), context), sp_manager_{sp_manager} {}

    void start() { do_handshake(); }

  private:
    void do_handshake() {
        stream_.async_handshake(ssl::stream_base::server,
            [self = shared_from_this()](std::error_code const& error) {
                if (!error) {
                    self->do_read();
                }
            } //
        );
    }

    void do_read() {
        async_read(stream_, dynamic_buffer(buffer_), messages::transfer_until_end_of_message{buffer_},
            [self = shared_from_this()](std::error_code const& ec, std::size_t /*length*/) {
                if (!ec) {
                    // SSL_get_peer_certificate() increments the reference counter of the X509 object
                    // -> it must be explicitly freed, so pass a unique_ptr with a custom deleter
                    self->buffer_ = self->sp_manager_.process_group_request(
                        self->buffer_, x509_ptr{SSL_get_peer_certificate(self->stream_.native_handle())});
                    self->do_write();
                }
            } //
        );
    }

    void do_write() {
        async_write(stream_, asio::buffer(buffer_, buffer_.size()),
            [self = shared_from_this()](const std::error_code& ec, std::size_t /*length*/) {
                if (!ec) {
                    self->do_shutdown();
                }
            });
    }

    void do_shutdown() {
        stream_.async_shutdown([self = shared_from_this()](std::error_code const& ec) {
            if (!ec) {
                std::cout << "shutdown without error.\n";
            } else {
                std::cout << "shutdown with error: " << ec.message() << "\n";
            }
        });
    }

    ssl::stream<ip::tcp::socket> stream_;
    security_policy_manager const& sp_manager_;
    std::vector<std::uint8_t> buffer_{};
};

int alpn_callback(SSL* /*ssl*/, const unsigned char** out, unsigned char* outlen, const unsigned char* in,
    unsigned int inlen, void* /*arg*/) noexcept {
    constexpr std::array<std::uint8_t, 8> alpn_proto{7, 'n', 't', 's', 'k', 'e', '/', '1'};

    // not using SSL_select_next_proto because it doesn't care about const
    auto const* found = std::search(in, in + inlen, begin(alpn_proto), end(alpn_proto));
    if (found != (in + inlen)) {
        *out = found;
        *outlen = sizeof(alpn_proto);
        return SSL_TLSEXT_ERR_OK;
    }
    return SSL_TLSEXT_ERR_NOACK;
}

tls_server::tls_server(io_context& io_context, uint16_t port, security_policy_manager const& sp_manager)
    : acceptor_{io_context, ip::tcp::endpoint{ip::tcp::v4(), port}}, context_{ssl::context::tlsv13_server},
      sp_manager_{sp_manager} {
    context_.set_options(ssl::context::no_sslv2 | ssl::context::no_sslv3 | ssl::context::no_tlsv1 |
                         ssl::context::no_tlsv1_1 | ssl::context::default_workarounds);

    context_.use_certificate_chain_file("server_cert_chain.pem");
    context_.use_private_key_file("server_key.pem", ssl::context::file_format::pem);

    context_.load_verify_file("root_ca.pem");
    context_.set_verify_mode(ssl::context::verify_peer | ssl::context::verify_fail_if_no_peer_cert);

    SSL_CTX_set_alpn_select_cb(context_.native_handle(), alpn_callback, nullptr);

    do_accept();
}

void tls_server::do_accept() {
    acceptor_.async_accept([this](std::error_code const& error, ip::tcp::socket socket) {
        if (!error) {
            std::make_shared<tls_session>(std::move(socket), context_, sp_manager_)->start();
        }

        do_accept();
    });
}

} // namespace ntske4ptp
