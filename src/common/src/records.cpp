#include "ntske4ptp/records.hpp"
#include "ntske4ptp/util.hpp"

namespace ntske4ptp::records {

constexpr std::uint8_t record_true = 0x00;
constexpr std::uint8_t record_false = 0xff;

struct record_body_size {
    using size_type = std::uint16_t;

    constexpr size_type operator()(end_of_message) const noexcept { return 0; }
    constexpr size_type operator()(error) const noexcept { return sizeof(error); }
    constexpr size_type operator()(warning) const noexcept { return sizeof(warning); }
    /*     */ size_type operator()(nts_next_protocol_negotiation const& nnpn) const noexcept {
        return static_cast<size_type>(nnpn.size() * sizeof(nts_next_protocol));
    }
    constexpr size_type operator()(sdoid_request) const noexcept { return sizeof(sdoid_request); }
    constexpr size_type operator()(ptp_domain_request) const noexcept { return sizeof(ptp_domain_request); }
    constexpr size_type operator()(group_access_indicator) const noexcept { return 1; }
    /*     */ size_type operator()(security_policy const& sp) const noexcept {
        return static_cast<size_type>(
            sp.size() * (sizeof(message_type) + sizeof(security_parameter_pointer)));
    }
    /*     */ size_type operator()(security_association const& sa) const noexcept {
        return static_cast<size_type>(    //
            sizeof(std::uint8_t)          // security parameter pointer
            + 1                           // immediate security
            + sizeof(std::uint16_t)       // sequence no length
            + sizeof(std::uint16_t)       // res length
            + sizeof(std::uint16_t)       // sequence id window
            + sizeof(std::uint16_t)       // integrity alg type length
            + sa.integrity_alg_typ.size() //
            + sizeof(std::uint16_t)       // icv length
            + sizeof(std::uint32_t)       // key id
            + sizeof(std::uint16_t)       // key length
            + sa.key.size());
    }
    constexpr size_type operator()(security_parameter_validity_period const&) const noexcept {
        return 2 * sizeof(std::uint64_t);
    }
    constexpr size_type operator()(security_parameter_update_time const&) const noexcept {
        return sizeof(std::uint64_t);
    }
};

[[nodiscard]] std::size_t record::length() const noexcept {
    return std::visit(record_body_size{}, body) + record_header_length;
}

struct record_body_serializer {
    std::uint8_t* out;

    template <typename BodyType>
    std::uint8_t* operator()(BodyType const&) const {
        throw std::runtime_error("Record body type not implemented.");
    }

    std::uint8_t* operator()(end_of_message) const noexcept { return out; }
    std::uint8_t* operator()(nts_next_protocol_negotiation const& nnpn) noexcept {
        for (auto protocol : nnpn) {
            out = util::serialize_integer(util::to_integer(protocol), out);
        }
        return out;
    }
    std::uint8_t* operator()(sdoid_request sdoid) noexcept {
        out = util::serialize_integer(sdoid, out);
        return out;
    }
    std::uint8_t* operator()(ptp_domain_request domain_number) noexcept {
        out = util::serialize_integer(domain_number, out);
        return out;
    }
    std::uint8_t* operator()(group_access_indicator group_access) noexcept {
        *out++ = group_access ? record_true : record_false;
        return out;
    }
    std::uint8_t* operator()(security_policy const& sp) noexcept {
        for (auto [msg_type, spp] : sp) {
            *out++ = util::to_integer(msg_type);
            *out++ = spp;
        }
        return out;
    }
    std::uint8_t* operator()(security_association const& sa) noexcept {
        *out++ = sa.spp;
        *out++ = sa.immediate_security ? record_true : record_false;
        out = util::serialize_integer(sa.sequenceno_length.value_or(0), out);
        out = util::serialize_integer(sa.res_length.value_or(0), out);
        out = util::serialize_integer(sa.sequenceid_window, out);
        out = util::serialize_integer(static_cast<std::uint16_t>(sa.integrity_alg_typ.size()), out);
        out = std::copy(begin(sa.integrity_alg_typ), end(sa.integrity_alg_typ), out);
        out = util::serialize_integer(sa.icv_length, out);
        out = util::serialize_integer(sa.key_id, out);
        out = util::serialize_integer(static_cast<std::uint16_t>(sa.key.size()), out);
        out = std::copy(begin(sa.key), end(sa.key), out);
        return out;
    }
    std::uint8_t* operator()(security_parameter_validity_period const& spvp) noexcept {
        using namespace std::chrono;
        for (auto tp : {spvp.not_before, spvp.not_after}) {
            out = util::serialize_integer(
                static_cast<std::uint64_t>(duration_cast<seconds>(tp.time_since_epoch()).count()), out);
        }
        return out;
    }
    std::uint8_t* operator()(security_parameter_update_time const& sput) noexcept {
        using namespace std::chrono;
        out = util::serialize_integer(
            static_cast<std::uint64_t>(duration_cast<seconds>(sput.time_since_epoch()).count()), out);
        return out;
    }
};

std::uint8_t* serialize_record(record const& rec, std::uint8_t* out) noexcept {
    auto type = util::to_integer(rec.type);
    if (rec.critical) {
        type |= 0x8000;
    }
    out = util::serialize_integer(type, out);

    out = util::serialize_integer(std::visit(record_body_size{}, rec.body), out);
    out = std::visit(record_body_serializer{out}, rec.body);

    return out;
}

auto deserialize_nts_next_protocol_negotiation(std::uint8_t const*& begin, std::uint8_t const* end) {
    nts_next_protocol_negotiation n;
    while (begin != end) {
        n.insert(static_cast<nts_next_protocol>(util::deserialize_integer<std::uint16_t>(begin)));
    }
    return n;
}

auto deserialize_sdoid_request(std::uint8_t const*& begin) noexcept {
    return util::deserialize_integer<std::uint16_t>(begin);
}

auto deserialize_ptp_domain_request(std::uint8_t const*& begin) noexcept { return *begin++; }

auto deserialize_group_access_indicator(std::uint8_t const*& begin) noexcept {
    return (*begin++ != record_false);
}

auto deserialize_security_policy(std::uint8_t const*& begin, std::uint8_t const* end) {
    security_policy sp;
    while (begin != end) {
        auto msg_type = static_cast<message_type>(*begin++);
        auto spp = *begin++;
        sp.insert_or_assign(msg_type, spp);
    }
    return sp;
}

auto deserialize_security_association(std::uint8_t const*& begin) {
    security_association sa;
    sa.spp = *begin++;
    sa.immediate_security = ((*begin++) != record_false);
    if (auto sequenceno_length = util::deserialize_integer<std::uint16_t>(begin); sequenceno_length != 0) {
        sa.sequenceno_length = sequenceno_length;
    }
    if (auto res_length = util::deserialize_integer<std::uint16_t>(begin); res_length != 0) {
        sa.res_length = res_length;
    }
    sa.sequenceid_window = util::deserialize_integer<std::uint16_t>(begin);
    auto integrity_alg_type_length = util::deserialize_integer<std::uint16_t>(begin);
    std::copy_n(begin, integrity_alg_type_length, std::back_inserter(sa.integrity_alg_typ));
    begin += integrity_alg_type_length;
    sa.icv_length = util::deserialize_integer<std::uint16_t>(begin);
    sa.key_id = util::deserialize_integer<std::uint32_t>(begin);
    auto key_length = util::deserialize_integer<std::uint16_t>(begin);
    std::copy_n(begin, key_length, std::back_inserter(sa.key));
    begin += key_length;
    return sa;
}

auto deserialize_security_parameter_validity_period(std::uint8_t const*& begin) noexcept {
    using namespace std::chrono;
    security_parameters::validity_period vp;
    vp.not_before = system_clock::time_point(seconds{util::deserialize_integer<std::uint64_t>(begin)});
    vp.not_after = system_clock::time_point(seconds{util::deserialize_integer<std::uint64_t>(begin)});
    return vp;
}

auto deserialize_security_parameter_update_time(std::uint8_t const*& begin) noexcept {
    using namespace std::chrono;
    return time_point(seconds{util::deserialize_integer<std::uint64_t>(begin)});
}

record deserialize_record(std::uint8_t const*& begin, std::uint8_t const* end) {
    record result;
    auto const buffer_length = static_cast<std::size_t>(std::distance(begin, end));
    if (buffer_length < record_header_length) {
        // error, invalid record
        throw std::runtime_error("record length greater than buffer");
    }

    result.critical = ((*begin) & 0x80) != 0;
    result.type = static_cast<record_type>(util::deserialize_integer<std::uint16_t>(begin) & 0x7fff);

    auto const body_length = util::deserialize_integer<std::uint16_t>(begin);
    if (buffer_length < (record_header_length + body_length)) {
        // error, invalid record
        throw std::runtime_error("record length greater than buffer");
    }

    switch (result.type) {
    case record_type::end_of_message: {
        result.body = end_of_message{};
    } break;
    case record_type::nts_next_protocol_negotiation: {
        result.body = deserialize_nts_next_protocol_negotiation(begin, begin + body_length);
    } break;
    case record_type::sdoid_request: {
        result.body = deserialize_sdoid_request(begin);
    } break;
    case record_type::ptp_domain_request: {
        result.body = deserialize_ptp_domain_request(begin);
    } break;
    case record_type::group_access_indicator:
    case record_type::next_group_access_indicator: {
        result.body = deserialize_group_access_indicator(begin);
    } break;
    case record_type::security_policy:
    case record_type::next_security_policy: {
        result.body = deserialize_security_policy(begin, begin + body_length);
    } break;
    case record_type::security_association:
    case record_type::next_security_association: {
        result.body = deserialize_security_association(begin);
    } break;
    case record_type::security_parameter_validity_period:
    case record_type::next_security_parameter_validity_period: {
        result.body = deserialize_security_parameter_validity_period(begin);
    } break;
    case record_type::security_parameter_update_time:
    case record_type::next_security_parameter_update_time: {
        result.body = deserialize_security_parameter_update_time(begin);
    } break;
    default: {
        throw std::runtime_error("Record type not implemented.");
    }
    }

    return result;
}

std::string_view to_string_view(record_type type) noexcept {
    switch (type) {
    case record_type::end_of_message:
        return "End of Message";
    case record_type::nts_next_protocol_negotiation:
        return "NTS Next Protocol Negotiation";
    case record_type::error:
        return "Error";
    case record_type::warning:
        return "Warning";
    case record_type::aead_algorithm_negotiation:
        return "AEAD Algorithm Negotiation";
    case record_type::new_cookie_for_ntpv4:
        return "New Cookie for NTPv4";
    case record_type::ntpv4_server_negotiation:
        return "NTPv4 Server Negotiation";
    case record_type::ntpv4_port_negotiation:
        return "NTPv4 Port Negotiation";
    case record_type::sdoid_request:
        return "SdoId Request";
    case record_type::ptp_domain_request:
        return "PTP Domain Request";
    case record_type::group_access_indicator:
        return "Group Access Indicator";
    case record_type::security_policy:
        return "Security Policy";
    case record_type::security_association:
        return "Security Association";
    case record_type::security_parameter_validity_period:
        return "Security Parameter Validity Period";
    case record_type::security_parameter_update_time:
        return "Security Parameter Update Time";
    case record_type::next_group_access_indicator:
        return "Next Group Access Indicator";
    case record_type::next_security_policy:
        return "Next Security Policy";
    case record_type::next_security_association:
        return "Next Security Association";
    case record_type::next_security_parameter_validity_period:
        return "Next Security Parameter Validity Period";
    case record_type::next_security_parameter_update_time:
        return "Next Security Parameter Update Time";
    }
    return "Unknown";
}

} // namespace ntske4ptp::records
