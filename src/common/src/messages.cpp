#include "ntske4ptp/messages.hpp"
#include "ntske4ptp/records.hpp"
#include <array>
#include <iostream>
#include <numeric>

namespace ntske4ptp::messages {

template <typename InputIterator>
auto serialize_records(InputIterator begin, InputIterator end) {
    using namespace records;
    auto const buffer_size = std::accumulate(
        begin, end, std::size_t{0}, [](std::size_t size, record const& rec) { return size + rec.length(); });
    std::vector<std::uint8_t> buffer(buffer_size);

    // for some reason, msvc marks std::accumulate as [[nodiscard]]
    [[maybe_unused]] auto _ = std::accumulate(begin, end, buffer.data(),
        [](std::uint8_t* out, record const& rec) { return serialize_record(rec, out); });

    return buffer;
}

std::vector<std::uint8_t> serialize_group_request(group_request request) {
    using namespace records;

    std::array const recs{
        record{true, record_type::nts_next_protocol_negotiation,
            nts_next_protocol_negotiation{nts_next_protocol::ptpv21}},
        record{true, record_type::sdoid_request, request.sdoid},
        record{true, record_type::ptp_domain_request, request.domain_number},
        record{true, record_type::end_of_message, end_of_message{}} //
    };

    return serialize_records(begin(recs), end(recs));
}

std::vector<uint8_t> serialize_group_response(std::optional<group_response>&& response) {
    using namespace records;

    auto group_access_granted = response.has_value();
    auto next_group_access_granted =
        group_access_granted &&
        std::holds_alternative<group_response::parameter_set>(response->next_parameters);

    // if (!group_access_granted) {
    //    // group access denied
    //    std::array const recs{
    //        record{true, record_type::nts_next_protocol_negotiation,
    //            nts_next_protocol_negotiation{nts_next_protocol::ptpv21}},
    //        record{true, record_type::group_access_indicator, group_access_indicator{false}},
    //        record{true, record_type::end_of_message, {}} //
    //    };
    //    return serialize_records(begin(recs), end(recs));
    //}

    std::size_t const record_count =
        6 + response->current_parameters.sas.size() +
        (next_group_access_granted
                ? 4 + std::get<group_response::parameter_set>(response->next_parameters).sas.size()
                : 1);

    std::vector<record> recs;
    recs.reserve(record_count);

    recs.push_back(record{true, record_type::nts_next_protocol_negotiation,
        nts_next_protocol_negotiation{nts_next_protocol::ptpv21}});

    recs.push_back(
        record{true, record_type::group_access_indicator, group_access_indicator{group_access_granted}});

    if (group_access_granted) {
        recs.push_back(
            record{true, record_type::security_policy, std::move(response->current_parameters.sp)});
        for (auto&& [_, sa] : response->current_parameters.sas) {
            recs.push_back(record{true, record_type::security_association, std::move(sa)});
        }
        recs.push_back(
            record{true, record_type::security_parameter_validity_period, response->current_parameters.vp});
        recs.push_back(record{
            true, record_type::security_parameter_update_time, response->current_parameters.update_time});

        if (auto* next_parameters = std::get_if<group_response::parameter_set>(&response->next_parameters);
            next_parameters != nullptr) {
            recs.push_back(
                record{true, record_type::next_group_access_indicator, group_access_indicator{true}});
            recs.push_back(record{true, record_type::next_security_policy, std::move(next_parameters->sp)});
            for (auto&& [_, sa] : next_parameters->sas) {
                recs.push_back(record{true, record_type::next_security_association, std::move(sa)});
            }
            recs.push_back(
                record{true, record_type::next_security_parameter_validity_period, next_parameters->vp});
            recs.push_back(
                record{true, record_type::next_security_parameter_update_time, next_parameters->update_time});
        } else if (std::holds_alternative<group_response::group_access_denied>(response->next_parameters)) {
            recs.push_back(
                record{true, record_type::next_group_access_indicator, group_access_indicator{false}});
        }
    }

    recs.push_back(record{true, record_type::end_of_message, {}});

    return serialize_records(begin(recs), end(recs));
}

std::vector<records::record> parse_message(std::vector<std::uint8_t> const& buffer) {
    using namespace records;

    // group_request: 4 records
    // group_response: at least 7 records
    // group_response with next parameters and one SA each: 12
    constexpr std::size_t likely_record_count = 12;

    std::vector<record> recs;
    recs.reserve(likely_record_count);

    auto* begin = buffer.data();
    auto const* end = begin + buffer.size();
    while (begin < end) {
        recs.emplace_back(deserialize_record(begin, end));
    }

    if (recs.front().type != record_type::nts_next_protocol_negotiation) {
        throw std::runtime_error("first record is not NTS Next Protocol Negotiation");
    }

    if (std::get<nts_next_protocol_negotiation>(recs.front().body).count(nts_next_protocol::ptpv21) == 0) {
        throw std::runtime_error("NTS Next Protocol Negotiation: unsupported protocol");
    }

    if (recs.back().type != record_type::end_of_message) {
        throw std::runtime_error("last record is not End Of Message");
    }

    return recs;
}

constexpr auto is_type(records::record_type type) noexcept {
    return [type](records::record const& rec) { return rec.type == type; };
}

template <typename Callable>
void find_record(
    std::vector<records::record> const& recs, records::record_type type, Callable&& c, bool required = true) {
    auto pos = find_if(begin(recs), end(recs), is_type(type));
    if (pos != end(recs)) {
        c(pos);
    } else if (required) {
        throw std::runtime_error(fmt::format("Required record missing: {}", type));
    }
}

template <typename Callable>
[[nodiscard]] auto find_record_evaluate(
    std::vector<records::record> const& recs, records::record_type type, Callable&& c) {
    auto pos = find_if(begin(recs), end(recs), is_type(type));
    if (pos != end(recs)) {
        return c(pos);
    }
    throw std::runtime_error(fmt::format("Required record missing: {}", type));
}

template <typename Callable>
void find_optional_record(std::vector<records::record> const& recs, records::record_type type, Callable&& c) {
    return find_record(recs, type, std::forward<Callable>(c), false);
}

template <typename Callable>
void find_recurring_record(
    std::vector<records::record> const& recs, records::record_type type, Callable&& c) {
    find_record(
        recs, type,
        [&](auto pos) {
            while (pos != end(recs)) {
                c(pos);
                pos++;
                pos = std::find_if(pos, end(recs), is_type(type));
            }
        },
        true);
}

// TODO: more efficient deserialization with a state machine
group_request deserialize_group_request(std::vector<std::uint8_t> const& request_buffer) {
    using namespace records;
    auto const recs = parse_message(request_buffer);
    group_request request;

    find_record(recs, record_type::sdoid_request, [&request](auto found) {
        request.sdoid = std::get<sdoid_request>(found->body); //
    });

    find_record(recs, record_type::ptp_domain_request, [&request](auto found) {
        request.domain_number = std::get<ptp_domain_request>(found->body); //
    });

    return request;
}

std::optional<group_response> deserialize_group_response(std::vector<std::uint8_t> const& response_buffer) {
    using namespace records;
    auto const recs = parse_message(response_buffer);
    group_response response;

    auto group_access = find_record_evaluate(recs, record_type::group_access_indicator,
        [&](auto found) { return std::get<group_access_indicator>(found->body); });
    if (!group_access) {
        return std::nullopt;
    }

    find_record(recs, record_type::security_policy, [&](auto found) {
        response.current_parameters.sp = std::move(std::get<security_policy>(found->body));
    });

    // at least one security association record must be present
    // TODO: check for SPPs from security policy?
    find_recurring_record(recs, record_type::security_association, [&](auto found) {
        auto& sa = std::get<security_association>(found->body);
        response.current_parameters.sas.emplace(sa.spp, std::move(sa));
    });

    find_record(recs, record_type::security_parameter_validity_period, [&](auto found) {
        response.current_parameters.vp = std::get<security_parameter_validity_period>(found->body);
    });

    find_record(recs, record_type::security_parameter_update_time, [&](auto found) {
        response.current_parameters.update_time = std::get<security_parameter_update_time>(found->body);
    });

    // if next security policy is present, try to find other next fields
    find_optional_record(recs, record_type::next_group_access_indicator, [&](auto found) {
        if (std::get<group_access_indicator>(found->body) == false) {
            response.next_parameters = group_response::group_access_denied{};
            return;
        }

        // next group access is given -> read next parameters
        auto next_parameters = group_response::parameter_set{};

        find_record(recs, record_type::next_security_policy,
            [&](auto it) { next_parameters.sp = std::move(std::get<security_policy>(it->body)); });

        // at least one next security association record must be present
        find_recurring_record(recs, record_type::next_security_association, [&](auto it) {
            auto& sa = std::get<security_association>(it->body);
            next_parameters.sas.emplace(sa.spp, std::move(sa));
        });

        find_record(recs, record_type::next_security_parameter_validity_period,
            [&](auto it) { next_parameters.vp = std::get<security_parameter_validity_period>(it->body); });

        find_record(recs, record_type::next_security_parameter_update_time, [&](auto it) {
            next_parameters.update_time = std::get<security_parameter_update_time>(it->body);
        });

        response.next_parameters = std::move(next_parameters);
    });

    return response;
}

transfer_until_end_of_message::transfer_until_end_of_message(
    std::vector<std::uint8_t> const& message_buffer) noexcept
    : message_buffer_{message_buffer} {}

std::size_t transfer_until_end_of_message::operator()(
    std::error_code const& error, std::size_t bytes_transferred) const noexcept {
    if (error) {
        return 0;
    }

    constexpr std::size_t minimum_message_buffer_size =
        4 * records::record_header_length + sizeof(records::nts_next_protocol) +
        sizeof(records::sdoid_request) + sizeof(records::ptp_domain_request);

    if (bytes_transferred < minimum_message_buffer_size) {
        return minimum_message_buffer_size - bytes_transferred;
    }

    if (std::equal(end(message_buffer_) - 4, end(message_buffer_), begin(records::end_of_message_bytes))) {
        return 0;
    }
    return 1;
}

} // namespace ntske4ptp::messages
