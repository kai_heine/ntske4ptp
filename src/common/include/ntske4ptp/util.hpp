#ifndef NTSKE4PTP_UTIL_HPP
#define NTSKE4PTP_UTIL_HPP

#include <climits>
#include <type_traits>

namespace ntske4ptp::util {

template <typename Enum, typename = std::enable_if_t<std::is_enum_v<Enum>>>
[[nodiscard]] constexpr auto to_integer(Enum e) noexcept {
    return static_cast<std::underlying_type_t<Enum>>(e);
}

template <typename Integer, typename OutputIterator, typename = std::enable_if_t<std::is_integral_v<Integer>>>
constexpr OutputIterator serialize_integer(Integer value, OutputIterator begin) noexcept {
    for (std::size_t i = 0; i < sizeof(Integer); ++i) {
        *begin = static_cast<unsigned char>(value >> ((sizeof(Integer) - i - 1) * CHAR_BIT)) & 0xff;
        ++begin;
    }

    return begin;
}

template <typename Integer, typename InputIterator, typename = std::enable_if_t<std::is_integral_v<Integer>>>
[[nodiscard]] constexpr auto deserialize_integer(InputIterator& begin) noexcept {
    Integer value = 0;
    auto const end = begin + static_cast<std::ptrdiff_t>(sizeof(Integer));
    while (begin != end) {
        value = static_cast<Integer>((value << CHAR_BIT) + *begin);
        ++begin;
    }
    return value;
}

} // namespace ntske4ptp::util

#endif
