#ifndef NTSKE4PTP_SECURITY_POLICY_HPP
#define NTSKE4PTP_SECURITY_POLICY_HPP

#include <chrono>
#include <cstdint>
#include <map>
#include <ntske4ptp/security_association.hpp>

namespace ntske4ptp {

enum class message_type : std::uint8_t {
    sync = 0x0,
    delay_req = 0x1,
    pdelay_req = 0x2,
    pdelay_resp = 0x3,
    // reserved 4-7
    follow_up = 0x8,
    delay_resp = 0x9,
    pdelay_resp_follow_up = 0xA,
    announce = 0xB,
    signaling = 0xC,
    management = 0xD,
    // reserved E-F
};

using security_policy = std::map<message_type, security_parameter_pointer>;

struct group_id {
    std::uint16_t sdoid;
    std::uint8_t domain_number;
};

inline constexpr bool operator<(group_id const& lhs, group_id const& rhs) noexcept {
    constexpr auto serialize = [](group_id const& gid) { return ((gid.sdoid << 8) + gid.domain_number); };
    return serialize(lhs) < serialize(rhs);
}

using security_policy_database = std::map<group_id, security_policy>;

using time_point = std::chrono::system_clock::time_point;

struct security_parameters {
    security_association_database sad;
    security_policy_database spd;
    struct validity_period {
        time_point not_before;
        time_point not_after;
    } validity;
    time_point update_time;
};

} // namespace ntske4ptp

#endif
