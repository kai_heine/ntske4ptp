#ifndef NTSKE4PTP_MESSAGES_HPP
#define NTSKE4PTP_MESSAGES_HPP

#include "ntske4ptp/security_policy.hpp"
#include <system_error>
#include <variant>

namespace ntske4ptp::messages {

using group_request = group_id;

// empty optional group response means group access denied
struct group_response {
    struct parameter_set {
        security_policy sp;
        security_association_database sas;
        security_parameters::validity_period vp;
        time_point update_time;
    };
    struct unavailable {};
    struct group_access_denied {};
    parameter_set current_parameters;
    std::variant<unavailable, parameter_set, group_access_denied> next_parameters;
};

std::vector<std::uint8_t> serialize_group_request(group_request request);
std::vector<std::uint8_t> serialize_group_response(std::optional<group_response>&& response);

std::optional<group_response> deserialize_group_response(std::vector<std::uint8_t> const& response_buffer);
group_request deserialize_group_request(std::vector<std::uint8_t> const& request_buffer);

// completion condition for asio read/write
// implemented as a function object because a function returning a lambda can't be defined out of line
// since the type of a lambda can't be named and i don't want to return a std::function
class transfer_until_end_of_message {
  public:
    explicit transfer_until_end_of_message(std::vector<std::uint8_t> const& message_buffer) noexcept;
    std::size_t operator()(std::error_code const& error, std::size_t bytes_transferred) const noexcept;

  private:
    std::vector<std::uint8_t> const& message_buffer_;
};

} // namespace ntske4ptp::messages

#endif
