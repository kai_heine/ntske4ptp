#ifndef NTSKE4PTP_RECORDS_HPP
#define NTSKE4PTP_RECORDS_HPP

#include "ntske4ptp/security_policy.hpp"
#include <array>
#include <fmt/format.h>
#include <set>
#include <variant>

namespace ntske4ptp::records {

// https://www.iana.org/assignments/nts/nts.xhtml

enum class record_type : std::uint16_t {
    end_of_message = 0,
    nts_next_protocol_negotiation = 1,
    error = 2,
    warning = 3,
    aead_algorithm_negotiation = 4,
    new_cookie_for_ntpv4 = 5,
    ntpv4_server_negotiation = 6,
    ntpv4_port_negotiation = 7,
    // private or experimental use
    sdoid_request = 16384,
    ptp_domain_request,
    group_access_indicator,
    security_policy,
    security_association,
    security_parameter_validity_period,
    security_parameter_update_time,
    // next_period_parameters
    next_group_access_indicator,
    next_security_policy,
    next_security_association,
    next_security_parameter_validity_period,
    next_security_parameter_update_time
};

std::string_view to_string_view(record_type type) noexcept;

enum class nts_next_protocol : std::uint16_t {
    ntpv4 = 0,
    // private or experimental use
    ptpv21 = 32768
};

enum class error : std::uint16_t {
    unrecognized_critical_record = 0,
    bad_request = 1,
    internal_server_error = 2
};

enum class warning : std::uint16_t {
    // there are none
};

constexpr std::size_t record_header_length = 4;

struct end_of_message {};
// error
// warning
using nts_next_protocol_negotiation = std::set<nts_next_protocol>;
using sdoid_request = std::uint16_t;
using ptp_domain_request = std::uint8_t;
using group_access_indicator = bool;
// security policy as is
// security association as is
using security_parameter_validity_period = security_parameters::validity_period;
using security_parameter_update_time = std::chrono::system_clock::time_point;

// only unique types needed
using record_body = std::variant<end_of_message, error, warning, nts_next_protocol_negotiation, sdoid_request,
    ptp_domain_request, group_access_indicator, security_policy, security_association,
    security_parameter_validity_period, security_parameter_update_time>;

constexpr std::array<std::uint8_t, 4> end_of_message_bytes{0x80, 0x00, 0x00, 0x00};

struct record {
    bool critical{true};
    record_type type{};
    record_body body;

    [[nodiscard]] std::size_t length() const noexcept;
};

std::uint8_t* serialize_record(record const& rec, std::uint8_t* out) noexcept;

record deserialize_record(std::uint8_t const*& begin, std::uint8_t const* end);

} // namespace ntske4ptp::records

template <>
struct fmt::formatter<ntske4ptp::records::record_type> : formatter<string_view> {
    template <typename FormatContext>
    auto format(ntske4ptp::records::record_type type, FormatContext& ctx) {
        return formatter<string_view>::format(ntske4ptp::records::to_string_view(type), ctx);
    }
};

#endif
