#ifndef NTSKE4PTP_SECURITY_ASSOCIATION_HPP
#define NTSKE4PTP_SECURITY_ASSOCIATION_HPP

#include <cstdint>
#include <map>
#include <optional>
#include <vector>

namespace ntske4ptp {

using security_parameter_pointer = std::uint8_t;

struct security_association {
    security_parameter_pointer spp{0};
    bool immediate_security{true};
    std::optional<std::uint16_t> sequenceno_length;
    std::optional<std::uint16_t> res_length;
    std::uint16_t sequenceid_window{16};
    std::vector<std::uint8_t> integrity_alg_typ;
    std::uint16_t icv_length{16};
    std::uint32_t key_id{0};
    std::vector<std::uint8_t> key;
};

using security_association_database = std::map<security_parameter_pointer, security_association>;

} // namespace ntske4ptp

#endif
