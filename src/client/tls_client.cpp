#include "tls_client.hpp"
#include <asio/connect.hpp>
#include <asio/read.hpp>
#include <asio/ssl/stream.hpp>
#include <iostream>
#include <ntske4ptp/messages.hpp>

namespace ntske4ptp {

using namespace asio;

tls_client_transceiver::tls_client_transceiver(std::string_view address, std::uint16_t port)
    : endpoint_{ip::make_address(address), port}, tls_context_{ssl::context::tlsv13_client} {
    tls_context_.load_verify_file("root_ca.pem");
    tls_context_.use_certificate_file("client_cert.pem", asio::ssl::context::file_format::pem);
    tls_context_.use_private_key_file("client_key.pem", asio::ssl::context::file_format::pem);
    tls_context_.set_verify_mode(asio::ssl::verify_peer);

    constexpr std::array<std::uint8_t, 8> alpn_proto{7, 'n', 't', 's', 'k', 'e', '/', '1'};
    SSL_CTX_set_alpn_protos(
        tls_context_.native_handle(), alpn_proto.data(), static_cast<unsigned int>(alpn_proto.size()));
}

std::vector<std::uint8_t> tls_client_transceiver::operator()(
    std::vector<std::uint8_t> const& request_buffer) {
    asio::ssl::stream<asio::ip::tcp::socket> stream{io_context_, tls_context_};

    ip::tcp::resolver resolver{io_context_};
    asio::connect(stream.lowest_layer(), resolver.resolve(endpoint_));

    stream.handshake(asio::ssl::stream_base::client);

    asio::write(stream, asio::buffer(request_buffer));

    std::vector<std::uint8_t> response_buffer;
    asio::read(stream, asio::dynamic_buffer(response_buffer),
        messages::transfer_until_end_of_message{response_buffer});

    stream.shutdown();

    return response_buffer;
}

} // namespace ntske4ptp
