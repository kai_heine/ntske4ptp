#include "tls_client.hpp"
#include <fmt/format.h>
#include <ntske4ptp/messages.hpp>
#include <ntske4ptp/security_association.hpp>
#include <ntske4ptp/security_policy.hpp>

/*

client:
    - store spd and sad (current, previous, next)
    - start: client sends request to get spd and sad -> current
    - update process:
        - new request between update_time and not_after
        - store next spd/sad in next
        - when not_after is reached:
            - current -> previous
            - next -> current
*/

namespace ntske4ptp {

class ke_client {

  private:
    security_parameters current_;
    security_parameters next_;
    security_parameters previous_;
};

template <typename Transceiver>
auto do_request(group_id group, Transceiver&& transceive) {
    auto const request_buffer = messages::serialize_group_request(group);
    auto const response_buffer = transceive(request_buffer);
    return messages::deserialize_group_response(response_buffer);
}

} // namespace ntske4ptp

int main() {
    try {
        ntske4ptp::tls_client_transceiver tls{"127.0.0.1", 8888};
        auto const response = do_request({123, 42}, tls);
    } catch (std::exception const& e) {
        fmt::print("Error: {}\n", e.what());
    }
}
