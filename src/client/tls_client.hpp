#ifndef NTSKE4PTP_CLIENT_TLS_CLIENT_HPP
#define NTSKE4PTP_CLIENT_TLS_CLIENT_HPP

#include <asio/io_context.hpp>
#include <asio/ip/tcp.hpp>
#include <asio/ssl/context.hpp>
#include <string_view>

namespace ntske4ptp {

class tls_client_transceiver {
  public:
    tls_client_transceiver(std::string_view address, std::uint16_t port);

    [[nodiscard]] std::vector<std::uint8_t> operator()(std::vector<std::uint8_t> const& buffer);

  private:
    asio::io_context io_context_;
    asio::ip::tcp::endpoint endpoint_;
    asio::ssl::context tls_context_;
};

} // namespace ntske4ptp

#endif
