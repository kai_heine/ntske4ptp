#include <catch2/catch.hpp>
#include <ntske4ptp/messages.hpp>

using namespace ntske4ptp;

// TODO: next parameters

auto const group_request_input = messages::group_request{0x0123, 0x42};
auto const group_request_output = std::vector<std::uint8_t>{
    0x80, 0x01, 0x00, 0x02, 0x80, 0x00, // nts next proto
    0xC0, 0x00, 0x00, 0x02, 0x01, 0x23, // sdoid req
    0xC0, 0x01, 0x00, 0x01, 0x42,       // ptp domain req
    0x80, 0x00, 0x00, 0x00              // end of msg
};

TEST_CASE("group request serialization", "messages") {
    auto buffer = messages::serialize_group_request(messages::group_request{group_request_input});
    REQUIRE(buffer == group_request_output);
}

TEST_CASE("group request deserialization", "messages") {
    auto request = messages::deserialize_group_request(std::vector<std::uint8_t>{group_request_output});
    REQUIRE(request.sdoid == group_request_input.sdoid);
    REQUIRE(request.domain_number == group_request_input.domain_number);
}

auto const spp = security_parameter_pointer{0xA5};

auto const group_response_input = messages::group_response{
    messages::group_response::parameter_set{
        security_policy{
            {message_type::sync, spp},     //
            {message_type::delay_req, spp} //
        },
        security_association_database{
            {spp,
                security_association{
                    spp,                                                                //
                    true,                                                               //
                    {},                                                                 //
                    {},                                                                 //
                    16,                                                                 //
                    {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18}, //
                    16,                                                                 //
                    0x12345678, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16} //
                }}                                                                      //
        },
        security_parameters::validity_period{}, {} // update_time
    },
    {} // next parameters
};

auto const group_response_output = std::vector<std::uint8_t>{
    0x80, 0x01, 0x00, 0x02, 0x80, 0x00,                                   // nts next proto
    0xC0, 0x02, 0x00, 0x01, 0x00,                                         // group access indicator
    0xC0, 0x03, 0x00, 0x04, 0x00, spp, 0x01, spp,                         // security policy
    0xC0, 0x04, 0x00, 53,                                                 // security association
    spp,                                                                  //
    0x00,                                                                 //
    0x00, 0x00,                                                           //
    0x00, 0x00,                                                           //
    0x00, 16,                                                             //
    0, 19,                                                                //
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,     //
    0, 16,                                                                //
    0x12, 0x34, 0x56, 0x78,                                               //
    0, 16,                                                                //
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,                //
    0xC0, 0x05, 0x00, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // validity period
    0xC0, 0x06, 0x00, 8, 0, 0, 0, 0, 0, 0, 0, 0,                          // update time
    0x80, 0x00, 0x00, 0x00                                                // end of msg
};

TEST_CASE("group response serialization", "messages") {
    auto buffer = messages::serialize_group_response(messages::group_response{group_response_input});
    REQUIRE(buffer == group_response_output);
}

TEST_CASE("group response deserialization", "messages") {
    auto response = messages::deserialize_group_response(std::vector<std::uint8_t>{group_response_output});

    REQUIRE(response->current_parameters.sp == group_response_input.current_parameters.sp);

    REQUIRE(response->current_parameters.sas.count(spp) ==
            group_response_input.current_parameters.sas.count(spp));
    REQUIRE(response->current_parameters.sas.at(spp).spp ==
            group_response_input.current_parameters.sas.at(spp).spp);
    REQUIRE(response->current_parameters.sas.at(spp).immediate_security ==
            group_response_input.current_parameters.sas.at(spp).immediate_security);
    REQUIRE(response->current_parameters.sas.at(spp).sequenceno_length ==
            group_response_input.current_parameters.sas.at(spp).sequenceno_length);
    REQUIRE(response->current_parameters.sas.at(spp).res_length ==
            group_response_input.current_parameters.sas.at(spp).res_length);
    REQUIRE(response->current_parameters.sas.at(spp).sequenceid_window ==
            group_response_input.current_parameters.sas.at(spp).sequenceid_window);
    REQUIRE(response->current_parameters.sas.at(spp).integrity_alg_typ ==
            group_response_input.current_parameters.sas.at(spp).integrity_alg_typ);
    REQUIRE(response->current_parameters.sas.at(spp).icv_length ==
            group_response_input.current_parameters.sas.at(spp).icv_length);
    REQUIRE(response->current_parameters.sas.at(spp).key_id ==
            group_response_input.current_parameters.sas.at(spp).key_id);
    REQUIRE(response->current_parameters.sas.at(spp).key ==
            group_response_input.current_parameters.sas.at(spp).key);

    REQUIRE(
        response->current_parameters.vp.not_after == group_response_input.current_parameters.vp.not_after);
    REQUIRE(
        response->current_parameters.vp.not_before == group_response_input.current_parameters.vp.not_before);

    REQUIRE(response->current_parameters.update_time == group_response_input.current_parameters.update_time);
}

// TODO: test next period parameters
