#include <benchmark/benchmark.h>
#include <ntske4ptp/messages.hpp>

using namespace ntske4ptp;

auto const group_request_output = std::vector<std::uint8_t>{
    0x80, 0x01, 0x00, 0x02, 0x80, 0x00, // nts next proto
    0xC0, 0x00, 0x00, 0x02, 0x01, 0x23, // sdoid req
    0xC0, 0x01, 0x00, 0x01, 0x42,       // ptp domain req
    0x80, 0x00, 0x00, 0x00              // end of msg
};

auto const spp = security_parameter_pointer{0xA5};
auto const group_response_output = std::vector<std::uint8_t>{
    0x80, 0x01, 0x00, 0x02, 0x80, 0x00,                                   // nts next proto
    0xC0, 0x02, 0x00, 0x01, 0x00,                                         // group access indicator
    0xC0, 0x03, 0x00, 0x04, 0x00, spp, 0x01, spp,                         // security policy
    0xC0, 0x04, 0x00, 53,                                                 // security association
    spp,                                                                  //
    0x00,                                                                 //
    0x00, 0x00,                                                           //
    0x00, 0x00,                                                           //
    0x00, 16,                                                             //
    0, 19,                                                                //
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,     //
    0, 16,                                                                //
    0x12, 0x34, 0x56, 0x78,                                               //
    0, 16,                                                                //
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,                //
    0xC0, 0x05, 0x00, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // validity period
    0xC0, 0x06, 0x00, 8, 0, 0, 0, 0, 0, 0, 0, 0,                          // update time
    0x80, 0x00, 0x00, 0x00                                                // end of msg
};

static void parse_group_response(benchmark::State& state) {
    for (auto _ : state) {
        auto response = messages::deserialize_group_response(group_response_output);
        benchmark::DoNotOptimize(response);
    }
}

static void parse_group_request(benchmark::State& state) {
    for (auto _ : state) {
        auto request = messages::deserialize_group_request(group_request_output);
        benchmark::DoNotOptimize(request);
    }
}

BENCHMARK(parse_group_response);
BENCHMARK(parse_group_request);
