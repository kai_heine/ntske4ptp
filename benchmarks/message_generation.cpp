#include <benchmark/benchmark.h>
#include <ntske4ptp/messages.hpp>

using namespace ntske4ptp;

// TODO: next parameters

static void generate_group_response(benchmark::State& state) {
    messages::group_response const response{
        messages::group_response::parameter_set{
            security_policy{
                {message_type::sync, security_parameter_pointer{0xff}},     //
                {message_type::delay_req, security_parameter_pointer{0xff}} //
            },
            security_association_database{
                {security_parameter_pointer{0xff},
                    security_association{
                        security_parameter_pointer{0xff},                                   //
                        true,                                                               //
                        {},                                                                 //
                        {},                                                                 //
                        16,                                                                 //
                        {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18}, //
                        16,                                                                 //
                        1234, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}           //
                    }}                                                                      //
            },
            security_parameters::validity_period{
                std::chrono::system_clock::now(), std::chrono::system_clock::now() //
            },
            std::chrono::system_clock::now() // update_time
        },
        {} // next parameters
    };

    for (auto _ : state) {
        auto buffer = messages::serialize_group_response(messages::group_response{response});
        benchmark::DoNotOptimize(buffer.data());
        benchmark::ClobberMemory();
    }
}

static void generate_group_request(benchmark::State& state) {
    messages::group_request const request{1234, 1};

    for (auto _ : state) {
        auto buffer = messages::serialize_group_request(messages::group_request{request});
        benchmark::DoNotOptimize(buffer.data());
        benchmark::ClobberMemory();
    }
}

BENCHMARK(generate_group_response);
BENCHMARK(generate_group_request);
